<?php
/*
 * Plugin Name: Show products by category
 * Author: Author
 * Description: Hiển thị sản phẩm theo danh mục
 * Version: 0.0.1
 */

//Add
function ProductByCategory_create()
{
    register_widget('ProductByCategory');
}

add_action('widgets_init', 'ProductByCategory_create');

//Class content
class ProductByCategory extends WP_Widget
{

    function __construct()
    {
        parent::__construct(
            'product-by-category',
            'Show products by category',
            array('description' => 'Hiển thị sản phẩm theo danh mục')
        );
    }

    function form($instance)
    {
        $catid = !empty($instance['catid']) ? esc_attr($instance['catid']) : 0;
        $title = !empty($instance['title']) ? esc_attr($instance['title']) : '';
        $number = !empty($instance['number']) && intval($instance['number']) > 0 ? intval($instance['number']) : 3;
        
        $charcount = !empty($instance['charcount']) && intval($instance['charcount']) > 0 ? intval($instance['charcount']) : 300;
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
            <input id="<?php echo $this->get_field_id('title'); ?>" class="widefat title"
                   name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>">Number to show:</label>
            <input type="number" id="<?php echo $this->get_field_id('number'); ?>" class="widefat"
                   name="<?php echo $this->get_field_name('number'); ?>" value="<?php echo $number; ?>"/>
        </p>
        
        <p>
            <label for="<?php echo $this->get_field_id('charcount'); ?>">Maximum chars:</label>
            <input type="number" id="<?php echo $this->get_field_id('col'); ?>" class="widefat"
                   name="<?php echo $this->get_field_name('charcount'); ?>" value="<?php echo $charcount; ?>"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('catid'); ?>">Category:</label>
            <select id="<?php echo $this->get_field_id('catid'); ?>"
                    name="<?php echo $this->get_field_name('catid'); ?>">
                <option>--Select--</option>
                <?php
                $categories = get_categories(array(
                    'orderby' => 'name',
                    'order' => 'ASC'
                ));
                foreach ($categories as $category) {
                    ?>
                    <option <?php if ($catid == $category->cat_ID) echo ' selected'; ?>
                            value="<?php echo $category->cat_ID; ?>"><?php echo $category->name; ?></option>
                    <?php
                }
                ?>
            </select>
        </p>
        <?php
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['catid'] = esc_attr($new_instance['catid']);
        $instance['title'] = esc_attr($new_instance['title']);
        $instance['number'] = intval($new_instance['number']) > 0 ? intval($new_instance['number']) : 3;
        
        $instance['charcount'] = intval($new_instance['charcount']) > 0 ? intval($new_instance['charcount']) : 200;
        return $instance;
    }

    function widget($args, $instance)
    {
        $catid = intval($instance['catid']);
        $number = intval($instance['number']);
        
        $charcount = intval($instance['charcount']);
        $post_by_category = get_posts(array(
            'category' => $catid,
            'numberposts' => $number,
        ));
        $categories = get_categories(array());
        ?>
            <section id="<?php 
                foreach ($categories as $category) {
                    if ($catid == $category->cat_ID){ 
                        echo $category->slug;
                    }
                }
            ?>">; <?php
            echo '<div class="product-title">';
            echo '<h2>'
                . apply_filters('widget_title', $instance['title'])
                . '</h2>';
                foreach ($categories as $category) {
                    if ($catid == $category->cat_ID){ ?>
                        <a href="<?php echo get_term_link($category->slug, 'category'); ?>">
                        Xem tất cả</a>
                    <?php        
                    }
                }
            echo '</div>';
            ?>
           
            <div class="info-product owl-carousel">
                <?php
                foreach ($post_by_category as $post_by_cat) {
                    ?>
                        <div class="item">
                            <a href="<?php echo get_permalink($post_by_cat->ID); ?>">
                                <img src="<?php echo get_the_post_thumbnail_url($post_by_cat->ID, 'thumbnail'); ?>" alt="<?php echo $post_by_cat->post_title; ?>"/>
                                <h4><?php echo $post_by_cat->post_title; ?></h4>
                                
                                <div class="desc-product">
                                    <?php echo substr($post_by_cat->post_content, 0, $charcount); ?>
                                </div>
                            </a>
                        </div>
                    <?php
                }
                ?>
            </div>
            <?php
            echo $args['after_widget'];
        echo '</section>';
    }

}