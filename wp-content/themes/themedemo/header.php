<!DOCTYPE html>

<!--[if IE 8]>
<html <?php language_attributes(); ?> class="ie8"> <![endif]-->
<!--[if !IE]>
<html <?php language_attributes(); ?>> <![endif]-->

<head>
<meta charset="<?php bloginfo( 'charset' ); ?>"/>
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="profile" href="http://gmgp.org/xfn/11"/>
   <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"/>
   
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <header>
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="logo">
                            <a href="<?php echo home_url(); ?>">
                                <img src="https://cdn.nguyenkimmall.com/images/companies/_1/html/2017/T11/homepage/Logo_NK.svg?v=2020" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="search-box">
                            <input type="text" placeholder="Bạn cần tìm gì hôm nay ?">
                            <button><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="hotline pull-right">
                            <a href="tel:18006800">
                                <div class="icon">
                                    <i class="fa fa-phone fa-2x" aria-hidden="true"></i>
                                </div>
                                <p>
                                    <span>Hotline: 1800 6800 (Miễn phí)</span>
                                    <span>Mua hàng - Góp ý - Bảo hành </span>
                                </p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="menu-category">
                            <h3><i class="fa fa-bars" aria-hidden="true"></i>Danh mục sản phẩm</h3>
                            <?php 
                            /* menu category */
                            wp_nav_menu( array(
                                'theme_location' => 'primary-menu', //Menu location của bạn
                                'depth' => 2, //Số cấp menu đa cấp
                                'container' => '', //Thẻ bao quanh cặp thẻ ul
                                'container_class'=>'', //class của thẻ bao quanh cặp thẻ ul
                                'menu_class' => 'nav-menu', //class của thẻ ul
                                'walker' => new wp_bootstrap_navwalker()) //Cái này để nguyên, không thay đổi
                            );
                            ?>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="menu-service">
                            <?php 
                            /* menu service */
                            wp_nav_menu( array(
                                'theme_location' => 'menu-service', //Menu location của bạn
                                'depth' => 1, //Số cấp menu đa cấp
                                'container' => '', //Thẻ bao quanh cặp thẻ ul
                                'container_class'=>'', //class của thẻ bao quanh cặp thẻ ul
                                'walker' => new wp_bootstrap_navwalker()) //Cái này để nguyên, không thay đổi
                            );
                            ?>
                        </div>
                        <div class="slideshow">
                            <?php
                            echo do_shortcode('[smartslider3 slider=2]');
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>