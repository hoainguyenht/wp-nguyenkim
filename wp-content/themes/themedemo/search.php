<?php get_header(); ?>
<div id="content">
<div class="container">
    <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-12">
            <div id="main-content">
                <div class="search-info">
                    <?php
                        $search_query   = new WP_Query( 's=' . $s . '&showposts=-1' );
                        $search_keyword = esc_html( $s );
                        $search_count   = $search_query->post_count;
                        printf( __( '<span>Kết quả tìm kiếm cho </span> <h1>"%1$s"</h1>', 'themedemo' ), 
                        $search_keyword );
                    ?>
                </div>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <?php get_template_part( 'content', get_post_format() ); ?>
                <?php endwhile; ?>
                <?php themedemo_number_pagination(); ?>
                <?php else : ?>
                    <?php get_template_part( 'content', 'none' ); ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div id="sidebar">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</div>
</div>
<?php get_footer(); ?>