<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="entry-thumbnail">
    <?php themedemo_thumbnail( 'thumbnail' ); ?>
</div>
<header class="entry-header">
    <?php themedemo_entry_header(); ?>
    <?php themedemo_entry_meta(); ?>
</header>
<div class="entry-content">
    <?php themedemo_entry_content(); ?>
    <?php //is_single() ? themedemo_entry_tag() : '' ?>
</div>
</article>
