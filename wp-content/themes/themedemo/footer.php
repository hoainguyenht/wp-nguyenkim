    <footer>
        <div class="container">
            <div class="info-footer">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <ul class="info-comany">
                            <li class="title">Thông tin công ty</li>
                            <li><a href="#">Giới thiệu công ty</a></li>
                            <li><a href="#">Đối tác chiến lược</a></li>
                            <li><a href="#">Hệ thống trung tâm</a></li>
                            <li><a href="#">Tuyển dụng</a></li>
                            <li><a href="#">Liên hệ</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <ul class="policy">
                            <li class="title">Chính sách</li>
                            <li><a href="#">Giao nhận - lắp đặt</a></li>
                            <li><a href="#">Dùng thử sản phẩm</a></li>
                            <li><a href="#">Đổi trả sản phẩm</a></li>
                            <li><a href="#">Bảo hành</a></li>
                            <li><a href="#">Trả góp</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <ul class="support">
                            <li class="title">Hỗ trợ khách hàng</li>
                            <li><a href="#">Hướng dẫn mua hàng</a></li>
                            <li><a href="#">Tra cứu bảo hành</a></li>
                            <li><a href="#">Hóa đơn điện tử</a></li>
                            <li><a href="#">Mua hàng doanh nghiệp</a></li>
                            <li><a href="#">Câu hỏi thường gặp</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <ul class="contact-us">
                            <li class="title">Kết nối với chúng tôi</li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class="fa fa-facebook-official fa-3x" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class="fa fa-youtube-play fa-3x" aria-hidden="true"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>