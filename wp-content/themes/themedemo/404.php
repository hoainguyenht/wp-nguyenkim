<?php get_header(); ?>
<div id="content">
    <div class="container">
        <div class="row">
            <div id="main-content">
                <?php  
                    echo '<div class="form-404">';
                        _e('<h2>Error: 404 NOT FOUND</h2>', 'themedemo');
                        _e('<p>The article you were looking for was not found, but maytry looking again!</p>', 'themedemo');
                        get_search_form();
                        echo '<div class="back-home">';
                            echo '<a href="'. home_url() .'">Trở về trang chủ</a>';
                        echo '</div>';
                    echo '</div>';

                    echo '<div class="cat-list-404">';
                        wp_list_categories(array('title' => ''));
                    echo '</div>';

                    echo '<div class="tag-list-404">';
                        echo 'Cloud tags: ';
                        wp_tag_cloud();
                    echo '</div>';
                ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>