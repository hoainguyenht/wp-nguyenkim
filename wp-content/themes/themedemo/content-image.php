<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="entry-thumbnail">
    <?php themedemo_thumbnail( 'large' ); ?>
</div>
<header class="entry-header">
    <?php themedemo_entry_header(); ?>
    <?php 
        echo '<div class="entry-meta">';
        $attachmen = get_children(array('post_parent' => $post->ID));
        $attachmen_number = count($attachmen);
        printf( __('This image post contain %1$s photos','themedemo'), $attachmen_number);
        echo '</div>';
    ?>
</header>
<div class="entry-content">
    <?php themedemo_entry_content(); ?>
    <?php is_single() ? themedemo_entry_tag() : '' ?>
</div>
</article>