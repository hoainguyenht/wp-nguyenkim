<?php
   if(is_active_sidebar('main-sidebar'))
       dynamic_sidebar('main-sidebar');
   else
       echo _e(__('Please enable this widget!', 'demostyle'));
?>