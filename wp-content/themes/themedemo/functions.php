<?php 
//Bootstrap navbar support
require_once ('class-wp-bootstrap-navwalker.php');

define( 'THEME_URL', get_stylesheet_directory() ); //Duong dan toi thu muc theme
define( 'CORE', THEME_URL . '/core' ); //Duong dan toi thu muc Core chua file nguon
require_once CORE . '/init.php'; //Load file init.php trong thu muc Core

//Thay doi kich co file upload
@ini_set('upload_max_size', '64M');
@ini_set('post_max_size', '64M');
@ini_set('max_execution_time', '300');

//Set chieu rong phan noi dung
if ( ! isset( $content_width ) ) {
$content_width = 620;
}

//them cac thanh phan theme ho tro
if ( ! function_exists( 'themedemo_theme_setup' ) ) {
    function themedemo_theme_setup() {
  
       //Ho tro nhieu ngon ngu
       $language_folder = THEME_URL . '/languages';
       load_theme_textdomain( 'themedemo', $language_folder );
  
       //Them RSS feed link tu dong vao phan <head>
       add_theme_support( 'automatic-feed-links' );
  
       //Them anh mo ta cho bai dang
       add_theme_support( 'post-thumbnails' );
  
       //Them chuc nang title-tag vao phan <title>
       add_theme_support( 'title-tag' );
  
       //Them chuc nang hien thi post theo dang bai dang
       add_theme_support( 'post-formats', array( 'image', 'video', 'gallery', 'quote', 'link' ) );
  
       //Them custom background
       $default_background = array( 'default-color' => '#e8e8e8' );
       add_theme_support( 'custom-background', $default_background );
  
       //Them menu
       register_nav_menu( 'menu-service', __( 'Menu Service', 'themedemo' ) );
       register_nav_menu ( 'primary-menu', __('Primary Menu', 'themedemo' ) );
  
       //Them sidebar cho theme
       $sidebar = array(
          'name'         => __( 'Main Sidebar', 'themedemo' ),
          'id'           => 'main-sidebar',
          'description'  => 'Main sidebar of Themeone',
          'class'        => 'main-sidebar',
          'before_title' => '<h3 class="widgettitle">',
          'after_title'  => '</h3>'
       );
       register_sidebar( $sidebar );
    }
    add_action( 'init', 'themedemo_theme_setup' );
}

//Nếu muốn thêm 1 file css hoặc file js:
function my_scripts_enqueue() {
 
    wp_register_script( 'jquery-js', get_template_directory_uri() . '/js/jquery.min.js', 'all' );
    wp_enqueue_script( 'jquery-js' );

    wp_register_script( 'bootstrap-js', get_template_directory_uri() . '/bootstrap/js/bootstrap.min.js', 'all' );
    wp_enqueue_script( 'bootstrap-js' );

    wp_register_script( 'custom-js', get_template_directory_uri() . '/js/custom.js', 'all' );
    wp_enqueue_script( 'custom-js' );

    wp_register_script( 'carousel-js', get_template_directory_uri() . '/owlcarousel/owl.carousel.js', 'all' );
    wp_enqueue_script( 'carousel-js' );

    wp_register_style( 'carousel-css', get_template_directory_uri() . '/owlcarousel/owl.carousel.css', 'all' );
    wp_enqueue_style( 'carousel-css' );

    wp_register_style( 'bootstrap-css', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css', false, null, 'all' );
    wp_enqueue_style( 'bootstrap-css' );
    
    wp_register_style('main-style', get_template_directory_uri() . "/style.css", 'all');
    wp_enqueue_style('main-style');

    wp_register_style('font-style', get_template_directory_uri() . "/font/css/font-awesome.min.css", 'all');
    wp_enqueue_style('font-style');

  
  }
add_action( 'wp_enqueue_scripts', 'my_scripts_enqueue' );

//Thiet lap hien thi menu
if ( ! function_exists( 'themedemo_menu' ) ) {
  function themedemo_menu( $menu ) {
    $menu = array(
       'theme_location'  => $menu,
       'container'       => 'ul',
    
    );
    wp_nav_menu( $menu );
  }
}
//Thumbnail post
if ( ! function_exists( 'themedemo_thumbnail' ) ) {
   function themedemo_thumbnail( $size ) {
 
      // Chỉ hiển thumbnail với post không có mật khẩu
      if ( ! is_single() && has_post_thumbnail() && ! post_password_required() || has_post_format( 'image' ) ) : ?>
            <figure class="post-thumbnail"><?php the_post_thumbnail( $size ); ?></figure><?php
      endif;
   }
 }
 
 //Entry header post
 if ( ! function_exists( 'themedemo_entry_header' ) ) {
   function themedemo_entry_header() {
      if ( is_single() || is_page() ) :
         ?>
            <h2 class="entry-title">
                <a id="entry-post" href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
               <?php the_title(); ?>
                </a>
            </h2>
      <?php else: ?>
            <h3 class="entry-title">
                <a id="entry-post" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
               <?php the_title(); ?>
                </a>
            </h3>
      <?php
      endif;
   }
 }
 
 //Entry meta header post
 if ( ! function_exists( 'themedemo_entry_meta' ) ) {
   function themedemo_entry_meta() {
      if ( ! is_page() ) :
         echo '<ul class="entry-meta">';
         //Hien thi avatar
         //Hien thi thong tin: tac gia, thoi gian, loai
         //printf( __( '<li class="author"><i class="fa fa-user-o"></i> %1$s</li>', 'themedemo' ), get_the_author() );
         printf( __( '<li class="date-published"><i class="fa fa-clock-o"></i> %1$s</li>', 'themedemo' ), get_the_date() );
         printf( __( '<li class="category"><i class="fa fa-list"></i> %1$s</li>', 'themedemo' ), get_the_category_list(',') );
         //Hien thi so binh luan
         // if ( comments_open() ):
         //    echo ' <li class="meta-reply"><i class="fa fa-comment"></i>';
         //    comments_popup_link(
         //       __( 'Leave a comment', 'themedemo' ),
         //       __( 'One comment', 'themedemo' ),
         //       __( '%  comments', 'themedemo' ),
         //       __( 'Read all comments', 'themedemo' )
         //    );
         //    echo '</li>';
         // endif;
         echo '</ul>';
      endif;
   }
 }
 
 //Hien thi noi dung bai dang: o trang chu -> hien thi tom tat, o trang noi dung: hien thi day du
 if ( ! function_exists( 'themedemo_entry_content' ) ) {
   function themedemo_entry_content() {
      if ( ! is_single() && ! is_page() ) :
         the_excerpt();
      else:
         the_content();
         $link_pages = array(
            'before'           => __( '<p>Page: ', 'themedemo' ),
            'after'            => '</p>',
            'nextpagelink'     => __( 'Next page', 'themedemo' ),
            'previouspagelink' => __( 'Previous page', 'themedemo' )
         );
         wp_link_pages( $link_pages );
      endif;
   }
 }

?>