<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp-test' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'K`+uJn(lDf#N2}2*u(H1z&j;+An:2K !>-jO(*]os%kMd<*-|uV`P;4t6Mzb)(h[' );
define( 'SECURE_AUTH_KEY',  'F+%,V_Dd;OV[zSper{>QX4rW+=69)0sz4_.hm92RMAHCJwT16,AmS;jC;c}Ryu24' );
define( 'LOGGED_IN_KEY',    '!B5[5<BU*;FR%LLog3A+K;Nk$T27$2FeR@K(ISm,2]#A$|AhI`Vz[hH~0:Dp=Z{X' );
define( 'NONCE_KEY',        '`At`pX&<7i7?1kF>ssGLJ}>]WI&Ajb .8AbgCbD1>I`1I-f=z%hdch^)m._JyhQ%' );
define( 'AUTH_SALT',        '|@|CV9xFOj_)@c5>:b+N8zZVI1<3=|Qj_xQ:Z(S)zhGik;mk>uI>H4$=<tj% ddI' );
define( 'SECURE_AUTH_SALT', 'eogu7!#i^2tkWqA~T7N{e0F(yz!y)6`yftv<qN3a+SQ&4VkAuG1K.:}3x%e,h=/s' );
define( 'LOGGED_IN_SALT',   'T2ag4lP{/^]mU&AK1wlD7GW+P;p^/{KVB!Dve~nk<d<WrL,~%cq%,E*zxD`4a89Z' );
define( 'NONCE_SALT',       'k@=UXX2u<tm40Rpv,g(IZR^hT8X|`o)<lZ 2mfhMfxxQ}.UQYP0zxEp1Mwy$:)p2' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
